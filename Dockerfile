FROM node:7.10-onbuild
VOLUME ['/usr/src/app']
WORKDIR /usr/src/app
RUN npm install --silent
RUN npm install -g serve webpack --silent
EXPOSE 80
RUN npm run build
CMD serve -s -p 80
