var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: "./dev/index.js",
    output: { 
        path: __dirname + "/public/", 
        filename: "index.js",
        publicPath: '/'
    },
    module: {
        loaders: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    cacheDirectory: false
                }
            }
        ]
    },
    devServer: {
        historyApiFallback: true,
    },
    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            template: 'index.html',
        }),
    ],
    node: {
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    }
};