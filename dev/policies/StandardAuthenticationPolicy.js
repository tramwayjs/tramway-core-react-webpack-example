import {policies} from "tramway-core";
let {AuthenticationStrategy} = policies;

/**
 * @export
 * @class StandardAuthenticationPolicy
 * @extends {AuthenticationStrategy}
 */
export default class StandardAuthenticationPolicy extends AuthenticationStrategy {
    /**
     * Creates an instance of StandardAuthenticationPolicy.
     * @memberOf StandardAuthenticationPolicy
     */
    constructor() {
        super('login');
    }

    /**
     * @param {function(Error, Object)} cb
     * @returns {function(Error, Object)}
     * @memberOf StandardAuthenticationPolicy
     */
    login(cb) {
        return cb(null, true);
    }

    /**
     * @param {function(Error, Object)} cb
     * @returns {function(Error, Object)}
     * @memberOf StandardAuthenticationPolicy
     */
    logout(cb) {
        return cb(null, true);
    }

    /**
     * @param {function(Error, Object)} cb
     * @returns {function(Error, Object)}
     * @memberOf StandardAuthenticationPolicy
     */
    check(cb) {
        return cb(null, true);
    }
}