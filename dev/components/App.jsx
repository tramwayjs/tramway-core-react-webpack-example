import React, {Component} from 'react';

/**
 * @class App
 * @extends {Component}
 */
class App extends Component {   
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <p>{this.props.message}</p>
        );
    }
}

App.defaultProps = {
    'message': 'This can be overloaded'
};

export default App;