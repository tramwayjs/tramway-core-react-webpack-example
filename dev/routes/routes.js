import MainController from "../controllers/MainController";
import PageNotFoundController from "../controllers/PageNotFoundController";
import HelloController from "../controllers/HelloController";
import StandardAuthenticationPolicy from "../policies/StandardAuthenticationPolicy";

const routesValues = [
    {
        "controller": MainController
    },
    {
        "path": "/hello",
        "arguments": ["name"],
        "controller": HelloController
    },
    {
        "controller": PageNotFoundController
    }
];

export default routesValues;